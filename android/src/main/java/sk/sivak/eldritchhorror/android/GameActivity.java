package sk.sivak.eldritchhorror.android;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidFiles;
import com.badlogic.gdx.pay.android.googlebilling.PurchaseManagerGoogleBilling;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.analytics.ExceptionReporter;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import java8.features.function.Consumer;
import main.java.sk.sivak.eldritchhorror.android.InterstitialAdListener;
import main.java.sk.sivak.eldritchhorror.android.JustRateThisGame;
import rx.Completable;
import rx.Single;
import sk.sivak.eldritchhorror.core.Game;
import sk.sivak.eldritchhorror.core.constants.AdHandler;
import sk.sivak.eldritchhorror.core.constants.tracker.AnalyticsCategory;
import sk.sivak.eldritchhorror.core.constants.tracker.AnalyticsTracker;
import sk.sivak.eldritchhorror.core.constants.tracker.GoogleServicesHolder;

import java.util.concurrent.TimeUnit;

public class GameActivity extends AndroidApplication {

    public static final String AD_TOKEN = "ca-app-pub-8462225877949835/5760614172";
    // ca-app-pub-8462225877949835~1124413940
    public static final String INTERSTETIAL_AD = "ca-app-pub-8462225877949835/6735833279";
    private RewardedVideoAd rewardedVideoAd;
    private Game game;
    private InterstitialAd mInterstitialAd;
    private AnalyticsTrackerImpl analyticsTracker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        game = new Game();
        initialize(game, config);

        try {
            ((AndroidFiles) Gdx.files).setAPKExpansion(1702, 0);
        } catch (Exception e) {

        }

        // NEW
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},0);
            return;
        }
        startWithPermissions();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        startWithPermissions();
    }

    public void startWithPermissions() {
        GoogleAnalytics googleAnalytics = GoogleAnalytics.getInstance(this);
        Tracker tracker = googleAnalytics.newTracker("UA-129188973-1");
        ExceptionReporter exceptionReporter = new ExceptionReporter(tracker,
                Thread.getDefaultUncaughtExceptionHandler(), this);

        analyticsTracker = new AnalyticsTrackerImpl(tracker, exceptionReporter);
        game.setAnalyticsTracker(analyticsTracker);

        MobileAds.initialize(this,
                "ca-app-pub-8462225877949835~1124413940");

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-8462225877949835/6735833279");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new InterstitialAdListener(this, mInterstitialAd, analyticsTracker));

        rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        rewardedVideoAd.loadAd(AD_TOKEN, new AdRequest.Builder().build());

        JustRateThisGame justRateThisGame = new JustRateThisGame(this);
        GoogleServicesHolder.setOpenStoreAction(justRateThisGame::openStore);
        GoogleServicesHolder.setAskLaterAction(justRateThisGame::askLater);
        GoogleServicesHolder.setShouldShowRateDialogSupplier(justRateThisGame::shouldShowRateDialog);

        game.setAdHandler(new AdHandlerImpl());

        game.setPurchaseManager(new PurchaseManagerGoogleBilling(this));

        Gdx.app.postRunnable(() -> {
            game.start();
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (rewardedVideoAd != null) {
            rewardedVideoAd.resume(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (rewardedVideoAd != null) {
            rewardedVideoAd.pause(this);
        }
    }

    private class AdHandlerImpl implements AdHandler {

        private Runnable onAdLoadedAction = () -> {};
        private Runnable onAdOpenedAction = () -> {};
        private Runnable onAdStartedAction = () -> {};
        private Runnable onAdClosedAction = () -> {};
        private Runnable onAdRewardedAction = () -> {};
        private Runnable onAdLeftApplicationAction = () -> {};
        private Consumer<Integer> onAdFailedToLoadAction = (errorCode) -> {};
        private Runnable onAdCompletedAction = () -> {};

        AdHandlerImpl() {
            rewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
                @Override
                public void onRewardedVideoAdLoaded() {
                    onAdLoadedAction.run();
                }

                @Override
                public void onRewardedVideoAdOpened() {
                    onAdOpenedAction.run();
                }

                @Override
                public void onRewardedVideoStarted() {
                    onAdStartedAction.run();
                }

                @Override
                public void onRewardedVideoAdClosed() {
                    onAdClosedAction.run();
                    reloadVideoAd();
                }

                @Override
                public void onRewarded(RewardItem rewardItem) {
                    onAdRewardedAction.run();
                }

                @Override
                public void onRewardedVideoAdLeftApplication() {
                    onAdLeftApplicationAction.run();
                }

                @Override
                public void onRewardedVideoAdFailedToLoad(int i) {
                    reloadVideoAd();
                    onAdFailedToLoadAction.accept(i);

                }

                @Override
                public void onRewardedVideoCompleted() {
                    onAdCompletedAction.run();
                }
            });
        }

        @Override
        public Single<Boolean> isRewardedVideoAdLoaded() {
            return Single.create(onSub -> {
                runOnUiThread(() -> {
                    onSub.onSuccess(rewardedVideoAd.isLoaded());
                });
            });
        }

        @Override
        public void showRewardedAd() {
            runOnUiThread(() -> {
                if (rewardedVideoAd.isLoaded()) {
                    rewardedVideoAd.show();
                }
            });
        }

        private void reloadVideoAd() {
            Completable.complete().delay(5, TimeUnit.SECONDS).subscribe(() -> {
                runOnUiThread(() -> {
                    rewardedVideoAd.loadAd(AD_TOKEN, new AdRequest.Builder().build());
                });
            });
        }

        // INTERSTITIAL
        private long adShownTime = System.currentTimeMillis();
        @Override
        public void showInterstitialAd() {
            runOnUiThread(() -> {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                    adShownTime = System.currentTimeMillis();
                } else {
                    analyticsTracker.trackInteraction(AnalyticsCategory.AD_MOB, "InterstitialAd", "isNotLoaded");
                    mInterstitialAd.loadAd(new AdRequest.Builder().build());
                }
            });
        }

        @Override
        public void showInterstitialAdAfterThreeMinutes() {
            runOnUiThread(() -> {
                if (System.currentTimeMillis() - adShownTime < 3*60*1000) {
                    return;
                }
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                    adShownTime = System.currentTimeMillis();
                } else {
                    analyticsTracker.trackInteraction(AnalyticsCategory.AD_MOB, "InterstitialAd", "isNotLoaded");
                    mInterstitialAd.loadAd(new AdRequest.Builder().build());
                }
            });
        }

        @Override
        public void setOnAdOpenedAction(Runnable onAdOpenedAction) {
            this.onAdOpenedAction = onAdOpenedAction;
        }

        @Override
        public void setOnAdLoadedAction(Runnable onAdLoadedAction) {
            this.onAdLoadedAction = onAdLoadedAction;
        }

        @Override
        public void setOnAdStartedAction(Runnable onAdStartedAction) {
            this.onAdStartedAction = onAdStartedAction;
        }

        @Override
        public void setOnAdClosedAction(Runnable onAdClosedAction) {
            this.onAdClosedAction = onAdClosedAction;
        }

        @Override
        public void setOnAdRewardedAction(Runnable onAdRewardedAction) {
            this.onAdRewardedAction = onAdRewardedAction;
        }

        @Override
        public void setOnAdLeftApplicationAction(Runnable onAdLeftApplicationAction) {
            this.onAdLeftApplicationAction = onAdLeftApplicationAction;
        }

        @Override
        public void setOnAdFailedToLoadAction(Consumer<Integer> onAdFailedToLoadAction) {
            this.onAdFailedToLoadAction= onAdFailedToLoadAction;
        }

        @Override
        public void setOnAdCompletedAction(Runnable onAdCompletedAction) {
            this.onAdCompletedAction = onAdCompletedAction;
        }
    }

    private class AnalyticsTrackerImpl implements AnalyticsTracker {

        private ExceptionReporter exceptionReporter;
        private Tracker tracker;

        AnalyticsTrackerImpl(Tracker tracker, ExceptionReporter exceptionReporter) {
            this.tracker = tracker;
            this.exceptionReporter = exceptionReporter;
        }

        @Override
        public void trackScreenName(String screenName) {
            tracker.setScreenName(screenName);
            tracker.send(new HitBuilders.ScreenViewBuilder().build());
        }

        @Override
        public void trackInteraction(AnalyticsCategory category, String action) {
            tracker.send(new HitBuilders.EventBuilder()
                    .setCategory(category.toString())
                    .setAction(action).build());
        }

        @Override
        public void trackInteraction(AnalyticsCategory category, String action, String label) {
            tracker.send(new HitBuilders.EventBuilder()
                    .setCategory(category.toString())
                    .setLabel(label)
                    .setAction(action).build());
        }

        @Override
        public void trackNonInteraction(AnalyticsCategory category, String action) {
            tracker.send(new HitBuilders.EventBuilder()
                    .setCategory(category.toString())
                    .setAction(action)
                    .setNonInteraction(true)
                    .build());
        }

        @Override
        public void trackNonInteraction(AnalyticsCategory category, String action, String label) {
            tracker.send(new HitBuilders.EventBuilder()
                    .setCategory(category.toString())
                    .setAction(action)
                    .setLabel(label)
                    .setNonInteraction(true)
                    .build());
        }

        @Override
        public void trackTiming(AnalyticsCategory category, String name, Long value) {
            tracker.send(new HitBuilders.TimingBuilder()
                    .setCategory(category.toString())
                    .setValue(value)
                    .setVariable(name)
                    .build());


        }

        @Override
        public void uncaughtException(Thread thread, Throwable throwable) {
            exceptionReporter.uncaughtException(thread, throwable);
        }
    }
}
