package main.java.sk.sivak.eldritchhorror.android;

import android.app.Activity;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import rx.Completable;
import rx.Observable;
import sk.sivak.eldritchhorror.core.constants.tracker.AnalyticsCategory;
import sk.sivak.eldritchhorror.core.constants.tracker.AnalyticsTracker;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class InterstitialAdListener extends AdListener {
    private final Activity activity;
    private final InterstitialAd mInterstitialAd;
    private final AnalyticsTracker analyticsTracker;

    public InterstitialAdListener(Activity activity, InterstitialAd mInterstitialAd, AnalyticsTracker analyticsTracker) {
        this.activity = activity;
        this.mInterstitialAd = mInterstitialAd;
        this.analyticsTracker = analyticsTracker;
    }

    @Override
    public void onAdClosed() {
        analyticsTracker.trackInteraction(AnalyticsCategory.AD_MOB, "InterstitialAd", "adClosed");
        reloadAd();
    }

    @Override
    public void onAdClicked() {
        analyticsTracker.trackInteraction(AnalyticsCategory.AD_MOB, "InterstitialAd", "adClicked");
    }

    @Override
    public void onAdFailedToLoad(int i) {
        analyticsTracker.trackInteraction(AnalyticsCategory.AD_MOB, "InterstitialAd", "adFailedToLoad-"+i);
        Completable.complete().delay(15, TimeUnit.SECONDS).subscribe(this::reloadAd);
    }

    private void reloadAd() {
        activity.runOnUiThread(() -> {
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
        });
    }

    @Override
    public void onAdLeftApplication() {
        analyticsTracker.trackInteraction(AnalyticsCategory.AD_MOB, "InterstitialAd", "adLeftApplication");
    }

    @Override
    public void onAdOpened() {
        analyticsTracker.trackInteraction(AnalyticsCategory.AD_MOB, "InterstitialAd", "adOpened");
    }

    @Override
    public void onAdLoaded() {
        analyticsTracker.trackInteraction(AnalyticsCategory.AD_MOB, "InterstitialAd", "adLoaded");
    }

    @Override
    public void onAdImpression() {
        analyticsTracker.trackInteraction(AnalyticsCategory.AD_MOB, "InterstitialAd", "adImpression");
    }
}
