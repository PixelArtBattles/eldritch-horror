package main.java.sk.sivak.eldritchhorror.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.text.TextUtils;
import com.kobakei.ratethisapp.RateThisApp;

public class JustRateThisGame {

    private final Activity activity;
    private boolean gameRated = false;
    private boolean later = false;

    public JustRateThisGame(Activity activity) {
        this.activity = activity;
        RateThisApp.onCreate(activity);
        RateThisApp.Config config = new RateThisApp.Config(1, 3);
        RateThisApp.init(config);
    }

    public boolean shouldShowRateDialog() {
        if (gameRated) {
            return false;
        }
        if (later) {
            return false;
        }
        return RateThisApp.shouldShowRateDialog();
    }

    public void openStore() {
        activity.runOnUiThread(() -> {
            String appPackage = activity.getPackageName();
            String url = "market://details?id=" + appPackage;
            try {
                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            } catch (android.content.ActivityNotFoundException anfe) {
                activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + activity.getPackageName())));
            }
            SharedPreferences pref = activity.getSharedPreferences("RateThisApp", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean("rta_opt_out", true);
            editor.apply();
            gameRated = true;
        });
    }

    public void askLater() {
        SharedPreferences pref = activity.getSharedPreferences("RateThisApp", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove("rta_install_date");
        editor.remove("rta_launch_times");
        editor.putLong("rta_ask_later_date", System.currentTimeMillis());
        later = true;
        editor.apply();
    }
}
