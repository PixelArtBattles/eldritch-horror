package sk.sivak.eldritchhorror.core.action;

/**
 * @author msivak
 */
public interface AfterEventAction<InOut> extends Action<InOut, InOut> {
}
