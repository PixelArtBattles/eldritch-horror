package sk.sivak.eldritchhorror.core.action;

/**
 * @author msivak
 */
public interface BeforeEventAction<InOut> extends Action<InOut, InOut> {
}
