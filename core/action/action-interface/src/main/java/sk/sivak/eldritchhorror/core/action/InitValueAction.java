package sk.sivak.eldritchhorror.core.action;

/**
 * @author msivak
 */
public interface InitValueAction<InOut> extends Action<InOut, InOut> {
}
