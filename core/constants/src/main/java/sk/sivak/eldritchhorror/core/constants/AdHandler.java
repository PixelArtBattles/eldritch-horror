package sk.sivak.eldritchhorror.core.constants;

import java8.features.function.Consumer;
import rx.Single;

public interface AdHandler {
    Single<Boolean> isRewardedVideoAdLoaded();

    void showRewardedAd();

    void showInterstitialAd();

    void showInterstitialAdAfterThreeMinutes();

    void setOnAdOpenedAction(Runnable onAdOpenedAction);

    void setOnAdLoadedAction(Runnable onAdLoadedAction);

    void setOnAdStartedAction(Runnable onAdStartedAction);

    void setOnAdClosedAction(Runnable onAdClosedAction);

    void setOnAdRewardedAction(Runnable onAdRewardedAction);

    void setOnAdLeftApplicationAction(Runnable onAdLeftApplicationAction);

    void setOnAdFailedToLoadAction(Consumer<Integer> onAdFailedToLoadAction);

    void setOnAdCompletedAction(Runnable onAdCompletedAction);
}
