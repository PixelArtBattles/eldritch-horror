package sk.sivak.eldritchhorror.core.constants;

/**
 * @author msivak
 */
public class GameProperties {
    public static final int MAX_TRAVEL_TICKETS = 2;
    public static final int MAX_FOCUS_TOKENS = 2;
}
