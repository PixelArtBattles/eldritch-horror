package sk.sivak.eldritchhorror.core.constants;

public interface GameRestarter {

    void restartGame();
}
