package sk.sivak.eldritchhorror.core.constants.card;

public interface CardId {

    String asString();
}
