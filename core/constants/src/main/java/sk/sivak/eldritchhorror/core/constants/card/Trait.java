package sk.sivak.eldritchhorror.core.constants.card;

/**
 * @author msivak
 */
public interface Trait {
    String asString();
}
