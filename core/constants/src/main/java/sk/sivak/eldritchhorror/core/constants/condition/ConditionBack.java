package sk.sivak.eldritchhorror.core.constants.condition;

public interface ConditionBack {
    String getTitle();

    String getFlavorText();

    int getConditionBackId();

    String getEffectText();
}
