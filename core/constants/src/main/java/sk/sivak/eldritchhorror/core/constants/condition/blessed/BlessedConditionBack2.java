package sk.sivak.eldritchhorror.core.constants.condition.blessed;

import sk.sivak.eldritchhorror.core.constants.condition.AbstractConditionBack;

public class BlessedConditionBack2 extends AbstractConditionBack {

    public BlessedConditionBack2() {
        title = "Reading the Stars";
        flavorText = "In the night sky, you can clearly see the predictions set forth by the stars, " +
                "and you realize there is still hope for mankind.";
        effectText = "[#GOOD]Select new Omen, Doom stays.\n" +
                "Then flip this card.[]";
    }
}
