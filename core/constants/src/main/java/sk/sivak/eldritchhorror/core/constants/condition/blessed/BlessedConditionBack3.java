package sk.sivak.eldritchhorror.core.constants.condition.blessed;

import sk.sivak.eldritchhorror.core.constants.condition.AbstractConditionBack;

public class BlessedConditionBack3 extends AbstractConditionBack {

    public BlessedConditionBack3() {
        title = "Light of Kthanid";
        flavorText = "In your moment of need, " +
                "the light of Kthanid shines down upon you, " +
                "filling your heart with courage and hope.";
        effectText = "[#GOOD]Retreat Doom.\n" +
                "Then flip this card.[]";
    }
}
