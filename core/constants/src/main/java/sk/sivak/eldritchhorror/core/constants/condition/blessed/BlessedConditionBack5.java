package sk.sivak.eldritchhorror.core.constants.condition.blessed;

import sk.sivak.eldritchhorror.core.constants.condition.AbstractConditionBack;

public class BlessedConditionBack5 extends AbstractConditionBack {

    public BlessedConditionBack5() {
        title = "Beloved of Bast";
        flavorText = "The ancient Egyptian goddess is moved by the kindness you have shown to stray cats. " +
                "Dozens of cats watch over you so you can rest safely.";
        effectText = "[#GOOD]Recover 2 Health and 2 Sanity.\n" +
                "Then flip this card.[]";
    }
}
