package sk.sivak.eldritchhorror.core.constants.condition.blessed;

import sk.sivak.eldritchhorror.core.constants.condition.AbstractConditionBack;

public class BlessedConditionBack6 extends AbstractConditionBack {

    public BlessedConditionBack6() {
        title = "Visions of Hypnos";
        flavorText = "In your dreams, a handsome youth shows you visions of faraway places " +
                "and the secrets that are hidden there.";
        effectText = "[#GOOD]Gain 2 Clues.\n" +
                "Then flip this card.[]";
    }
}
