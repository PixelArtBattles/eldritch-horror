package sk.sivak.eldritchhorror.core.constants.condition.blessed;

import sk.sivak.eldritchhorror.core.constants.condition.AbstractConditionBack;

public class BlessedConditionBack7 extends AbstractConditionBack {

    public BlessedConditionBack7() {
        title = "Blessing of Nodens";
        flavorText = "The Great Lord of the Abyss and his loyal nightgaunts are hunting the ancient one's servants. " +
                "Mighty Nodens looks upon you with favor.";
        effectText = "[#GOOD]Discard one Monster of your choice.\n" +
                "Then flip this card.[]";
    }
}
