package sk.sivak.eldritchhorror.core.constants.condition.cursed;

import sk.sivak.eldritchhorror.core.constants.condition.AbstractConditionBack;

public class CursedConditionBack7 extends AbstractConditionBack {

    public CursedConditionBack7() {
        title = "Karmic Justice";
        flavorText = "Perhaps you are not so forsaken. " +
                "You feel that the dark forces that have hounded you have now dissipated. " +
                "You do not question this sudden good fortune.";
        effectText = "[#GOOD]Discard this Card.[]";
    }
}
