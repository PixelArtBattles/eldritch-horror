package sk.sivak.eldritchhorror.core.constants.condition.darkpact;

import sk.sivak.eldritchhorror.core.constants.condition.AbstractConditionBack;

public class DarkPactConditionBack3 extends AbstractConditionBack {

    public DarkPactConditionBack3() {
        title = "Pact with Yog-Sothoth";
        flavorText = "The blasphemous power takes hold of you, " +
                "forcing you to carry out the desires of the Lurker at the Threshold.";
        effectText = "[#BAD]One Gate is spawned for each Spell you have,\n" +
                "then Omen advances[]\n" +
                "[#GOOD]Then discard this card.[]";
    }
}
