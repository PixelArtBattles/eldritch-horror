package sk.sivak.eldritchhorror.core.constants.condition.hallucinations;

import sk.sivak.eldritchhorror.core.constants.condition.AbstractConditionBack;

public class HallucinationsConditionBack4 extends AbstractConditionBack {

    public HallucinationsConditionBack4() {
        title = "Abducted";
        flavorText = "Everyone told you the beings were a figment of your imagination. " +
                "Now the creatures have come in your dreams to steal your soul away to another world.";
        effectText = "[#BAD]Gain a Lost in Time and Space Condition.[]\n" +
                "[#GOOD]Then discard this card.[]";
    }
}
