package sk.sivak.eldritchhorror.core.constants.difficulty;

/**
 * @author msivak
 */
public enum DifficultyId {
    EASY,
    NORMAL
}
