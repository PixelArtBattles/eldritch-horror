package sk.sivak.eldritchhorror.core.constants.displayasset;

public enum AssetOriginType {
    INVENTORY,
    CENTER
}
