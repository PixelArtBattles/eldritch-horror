package sk.sivak.eldritchhorror.core.constants.displayasset;

public enum DisplayAssetResponseType {
    YES_NO,
    OK,
    NOTHING
}
