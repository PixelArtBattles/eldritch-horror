package sk.sivak.eldritchhorror.core.constants.displayasset;

public enum HideType {
    RETURN,

    INVENTORY,
    INVENTORY_ON_YES,
    INVENTORY_ON_NO,

    DISABLE_ALWAYS,
    DISABLE_ON_YES,

    DISCARD_ALWAYS,
    DISCARD_ON_NO,
    DISCARD_ON_YES
}
