package sk.sivak.eldritchhorror.core.constants.displayasset;

public enum ShowCardResponse {
    OK,
    YES,
    NO,
    NOTHING
}
