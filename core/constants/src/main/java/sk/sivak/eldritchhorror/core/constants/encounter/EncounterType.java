package sk.sivak.eldritchhorror.core.constants.encounter;

public enum EncounterType {
    GENERAL,
    LOCATION,
    OTHER_WORLD,
    RLYEH_RISEN,
    VOID_BETWEEN_WORLDS,
    THE_KEY_AND_THE_GATE,
    EXPEDITION,
    MYSTERY,
    RUMOR,
    RESEARCH,
    COMBAT,
    CONDITION,
    DEFEATED_INVESTIGATOR,
    SKIP
}
