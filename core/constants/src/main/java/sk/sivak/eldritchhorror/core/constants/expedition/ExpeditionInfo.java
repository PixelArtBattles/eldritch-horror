package sk.sivak.eldritchhorror.core.constants.expedition;

import sk.sivak.eldritchhorror.core.constants.location.LocationId;

/**
 * @author msivak
 */
public interface ExpeditionInfo {

    LocationId getLocationId();

    int getPage();
}
