package sk.sivak.eldritchhorror.core.constants.gate;

import sk.sivak.eldritchhorror.core.constants.location.LocationId;

/**
 * @author msivak
 */
public interface GateInfo {

    GateColor getGateColor();

    LocationId getLocationId();
}
