package sk.sivak.eldritchhorror.core.constants.location;

/**
 * @author msivak
 */
public enum PathType {
    WALK,
    TRAIN,
    SHIP,
}
