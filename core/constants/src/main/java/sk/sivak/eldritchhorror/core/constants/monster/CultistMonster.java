package sk.sivak.eldritchhorror.core.constants.monster;

/**
 * @author msivak
 */
public class CultistMonster extends AbstractMonsterInfo {

    public CultistMonster() {
        super("Cultist",
                0, null, 0, null, null);
    }
}
