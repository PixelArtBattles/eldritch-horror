package sk.sivak.eldritchhorror.core.constants.monster;

import sk.sivak.eldritchhorror.core.constants.card.CardIdUtils;

public interface MonsterId {
    String asString();
}
