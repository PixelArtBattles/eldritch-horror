package sk.sivak.eldritchhorror.core.constants.monster;

/**
 * @author msivak
 */
public class StarVampireMonster extends AbstractMonsterInfo {

    public StarVampireMonster() {
        super("Star Vampire",
                -1, 3, -2, 2, 4);
        setSpawnText("Omen advances unless investigators as a group spend 2 Clues.");
    }
}
