package sk.sivak.eldritchhorror.core.constants.mythos;

public class BlueMythosCard1 extends BlueMythosCard {

    public BlueMythosCard1() {
        setRumorCardId("TheWindWalker");
        setTitle("The Wind-Walker");
        setFlavor("The weather grows worse. " +
                "Snow and ice cover cities that have never seen such weather before in their history.");
        addInfo("[#BAD]New Ongoing Rumor![]");
    }
}
