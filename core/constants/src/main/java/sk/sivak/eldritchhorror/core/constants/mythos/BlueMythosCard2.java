package sk.sivak.eldritchhorror.core.constants.mythos;

public class BlueMythosCard2 extends BlueMythosCard {

    public BlueMythosCard2() {
        setRumorCardId("StarsAligned");
        setTitle("Stars Aligned");
        setFlavor("A dozen unpleasant-looking strangers have settled into Panama " +
                "with a large collection of astronomical reference books. " +
                "There is something unique about this place and time that allows them to rip apart the fabric of reality.");
        addInfo("[#BAD]New Ongoing Rumor![]");
    }
}
