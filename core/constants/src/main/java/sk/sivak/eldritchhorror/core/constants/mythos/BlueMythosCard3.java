package sk.sivak.eldritchhorror.core.constants.mythos;

public class BlueMythosCard3 extends BlueMythosCard {

    public BlueMythosCard3() {
        setRumorCardId("FracturedReality");
        setTitle("Fractured Reality");
        setFlavor("The strange phenomenon is an echo of the catastrophic destruction of Mu. " +
                "The repercussions of the serpent people's overreaching ambition still take their toll.");
        addInfo("[#BAD]New Ongoing Rumor![]");
    }
}
