package sk.sivak.eldritchhorror.core.constants.mythos;

public class BlueMythosCard4 extends BlueMythosCard {

    public BlueMythosCard4() {
        setRumorCardId("LostKnowledge");
        setTitle("Lost Knowledge");
        setFlavor("Your contact in the capital is hesitant to speak about those called \"The Watches\". He says they work for a number of different governments, but answer to some other authority.");
        addInfo("[#BAD]New Ongoing Rumor![]");
    }
}
