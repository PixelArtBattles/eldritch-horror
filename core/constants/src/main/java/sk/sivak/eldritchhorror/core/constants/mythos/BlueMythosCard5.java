package sk.sivak.eldritchhorror.core.constants.mythos;

public class BlueMythosCard5 extends BlueMythosCard {

    public BlueMythosCard5() {
        setRumorCardId("GrowingMadness");
        setTitle("Growing Madness");
        setFlavor("You dream of a life as an insane wizard thousands of years ago in Atlantis. You also dream of that same wizard still alive today on an uncharted island.");
        addInfo("[#BAD]New Ongoing Rumor![]");
    }
}
