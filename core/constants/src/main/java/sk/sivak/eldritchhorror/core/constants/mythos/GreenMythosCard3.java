package sk.sivak.eldritchhorror.core.constants.mythos;

public class GreenMythosCard3 extends GreenMythosCard {

    public GreenMythosCard3() {
        setTitle("Blood Flows");
        setFlavor("\"Strong gris-gris,\" the old man told you. " +
                "You've been wearing the pouch around your neck ever since New Orleans. " +
                "\"You want it to spill blood fuh you,\" he drawled, " +
                "\"you has to spill your blood fuh the gris-gris.\"");
        addInfo("The Lead Investigator:\n" +
                "-[#GOOD]Discards one Monster of his choice[]\n" +
                "-[#BAD]Loses Health equal to its toughness[]");
    }
}
