package sk.sivak.eldritchhorror.core.constants.mythos;

public class GreenMythosCard4 extends GreenMythosCard {

    public GreenMythosCard4() {
        setTitle("Dimensional Instability");
        setFlavor("You cannot believe that this gibbering lunatic was once the smiling, " +
                "lucid lawyer you spoke to this morning. " +
                "\"Collapsed,\" his trembling hands indicated empty air. " +
                "\"The worlds collided, and the door collapsed!\" " +
                "He abruptly pulls himself off the floor and looks out the small window of his cell. " +
                "\"But so too will they pull. They will rip and tear, and a new door will open elsewhere.\"");
        addInfo("[#GOOD]Discard each Gate representing current Omen[]");
        addInfo("[#BAD]Advance Doom for each Gate discarded[]");
    }
}
