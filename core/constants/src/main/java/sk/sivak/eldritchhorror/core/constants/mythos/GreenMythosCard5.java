package sk.sivak.eldritchhorror.core.constants.mythos;

public class GreenMythosCard5 extends GreenMythosCard {

    public GreenMythosCard5() {
        setTitle("Calling the Elder Things");
        setFlavor("According to the police report, " +
                "several individuals entered the warehouse in the middle of the night. " +
                "They performed some sort of ritual which culminated in their death. " +
                "What you know that the police don't is that, afterward, something inhuman left that warehouse.");
        addInfo("[#BAD]Spawn one Monster on each space\n" +
                "that contains a Cultist Monster[]");
    }
}
