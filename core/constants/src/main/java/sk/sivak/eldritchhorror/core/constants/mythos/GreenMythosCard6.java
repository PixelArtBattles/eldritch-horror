package sk.sivak.eldritchhorror.core.constants.mythos;

public class GreenMythosCard6 extends GreenMythosCard {

    public GreenMythosCard6() {
        setTitle("Unexpected Betrayal");
        setFlavor("After weeks of encountering ancient and monstrous creatures, " +
                "you were unprepared for something so cold and personal as being betrayed by an associate. " +
                "Lies can be every bit as destructive as any wizard's curse.");
        addInfo("Each investigator\n" +
                "with one or more Allies:\n" +
                "-[#BAD]Loses three Health[]\n" +
                "-[#BAD]Discards one Ally[]");
    }
}
