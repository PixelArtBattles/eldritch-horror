package sk.sivak.eldritchhorror.core.constants.mythos;

public class GreenMythosCard8 extends GreenMythosCard {

    public GreenMythosCard8() {
        setMythosDifficulty(MythosDifficulty.EASY);
        setTitle("Support of the Church");
        setFlavor("You hang your head in exhaustion and frustration. " +
                "You jump suddenly when a hand rests on your shoulder. " +
                "To your relief, you turn to find the local priest. " +
                "\"Have faith, my child,\" he tells you. " +
                "\"Your good works have not gone unseen. " +
                "Your brave acts are making a difference.\"");
        addInfo("The Lead Investigator:\n" +
                "-[#GOOD]Gains a Blessed Condition[]");
    }
}
