package sk.sivak.eldritchhorror.core.constants.mythos;

public class YellowMythosCard1 extends YellowMythosCard {

    public YellowMythosCard1() {
        setTitle("Ancient Guardians");
        setFlavor("An article on the back page of the newspaper catches your attention. " +
                "An unexplained attack on a remote outpost left no survivors. " +
                "You've read about two other similar attacks happening in the same region. " +
                "You can see that the forgotten corners of the globe are growing more dangerous.");
        addInfo("[#BAD]Two Monsters are spawned\non the Active Expedition[]");
    }
}
