package sk.sivak.eldritchhorror.core.constants.mythos;

public class YellowMythosCard10 extends YellowMythosCard {

    public YellowMythosCard10() {
        setTitle("Heat Wave Singes the Globe");
        setFlavor("Sweating and exhausted, you stop to rest in what little shade you can find. " +
                "You've already heard stories of this heat wave claiming the lives of sick or elderly individuals. " +
                "You fear that if you keep going at this pace, you might fall victim to it as well.");
        addInfo("Each investigator:\n" +
                "-[#BAD]Loses three Health\n" +
                "unless he becomes Delayed[]");
    }
}
