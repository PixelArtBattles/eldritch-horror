package sk.sivak.eldritchhorror.core.constants.mythos;

public class YellowMythosCard11 extends YellowMythosCard {

    public YellowMythosCard11() {
        setMythosDifficulty(MythosDifficulty.EASY);
        setTitle("Omen of Good Fortune");
        setFlavor("You drop a penny into peculiar mechanical device. " +
                "The automaton waves its hand and turns over a card bearing the words,\n" +
                "\"Good luck is with you!\"");
        addInfo("[#GOOD]Select new Omen, Doom stays[]");
    }
}
