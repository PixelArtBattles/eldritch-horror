package sk.sivak.eldritchhorror.core.constants.mythos;

public class YellowMythosCard2 extends YellowMythosCard {

    public YellowMythosCard2() {
        setTitle("Terrible Truth");
        setFlavor("Now that you truly grasp the significance of what you have learned, " +
                "each scribbled note becomes unbearable to contemplate. " +
                "All the books you have read and the icons you have scrutinized " +
                "suddenly make sense as a single mind-shattering truth.");
        addInfo("For each Clue an investigator has,\n" +
                "[#BAD]that investigator discards that Clue\n" +
                "unless he spends one Health or one Sanity.[]");
    }
}
