package sk.sivak.eldritchhorror.core.constants.mythos;

public class YellowMythosCard3 extends YellowMythosCard {

    public YellowMythosCard3() {
        setTitle("Tide of Despair");
        setFlavor("A cold, bitter wind howls through the trees and " +
                "people everywhere cling to their fires and blankets. " +
                "The chill has settled into your bones, " +
                "and there's no warmth to be found.");
        addInfo("Each investigator:\n" +
                "-[#BAD]Loses two Health[]\n" +
                "-[#BAD]Loses two Sanity[]\n" +
                "[#BAD]unless he discards a Blessed Condition[]");
    }
}
