package sk.sivak.eldritchhorror.core.constants.mythos;

public class YellowMythosCard4 extends YellowMythosCard {

    public YellowMythosCard4() {
        setTitle("Haunting Nightmares");
        setFlavor("You wake up in mid-scream, and your heart is racing. " +
                "What little you can remember of your nightmare was so visceral and horrific " +
                "that you dread falling back asleep.");
        addInfo("Each investigator:\n" +
                "-[#BAD]Loses two Sanity[]\n" +
                "-[#BAD]Gains a Madness Condition\n" +
                "unless he spends one Clue[]");
    }
}
