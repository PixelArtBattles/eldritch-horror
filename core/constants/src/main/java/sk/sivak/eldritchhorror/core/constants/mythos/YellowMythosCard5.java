package sk.sivak.eldritchhorror.core.constants.mythos;

public class YellowMythosCard5 extends YellowMythosCard {

    public YellowMythosCard5() {
        setTitle("The World Shakes");
        setFlavor("The scope of the tremor was so vast " +
                "that newspapers in every country reported on the damage done " +
                "and the tragic destruction of ancient wonders.");
        addInfo("-[#BAD]Expedition location is destroyed[]\n"+
                "Each investigator on the Active Expedition\n" +
                "or an adjacent space:\n" +
                "-[#BAD]Loses two Health[]\n" +
                "-[#BAD]Becomes Delayed[]\n");
    }
}
