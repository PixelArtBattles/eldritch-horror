package sk.sivak.eldritchhorror.core.constants.mythos;

public class YellowMythosCard6 extends YellowMythosCard {

    public YellowMythosCard6() {
        setTitle("The Bermuda Triangle");
        setFlavor("These sudden thunderstorms have grown more common. " +
                "Twice a day, rain that smells like seawater floods the streets, " +
                "and cold winds that cut like glass threaten to pull doors off their hinges.");
        addInfo("Each investigator rolls one die.\n" +
                "On a 1 or 2:\n" +
                "-[#BAD]He moves to a Bermuda Triangle[]\n" +
                "-[#BAD]He becomes Delayed[]");
    }
}
