package sk.sivak.eldritchhorror.core.constants.mythos;

public class YellowMythosCard8 extends YellowMythosCard {

    public YellowMythosCard8() {
        setTitle("Arrests Made in Murder Case!");
        setFlavor("You open the door of your hotel to find several stone-faced policemen. " +
                "There's been a lot of unexplained deaths and nothing attracts suspicion like being a stranger.");
        addInfo("Each investigator on a City with a Weapon\n" +
                "tests Influence. If he fails:\n" +
                "-[#BAD]He discards one Weapon[]\n" +
                "-[#BAD]He gains a Detained Condition[]\n");
    }
}
