package sk.sivak.eldritchhorror.core.constants.mythos;

public class YellowMythosCard9 extends YellowMythosCard {

    public YellowMythosCard9() {
        setTitle("Rally the People");
        setFlavor("After the noise and violence is over, " +
                "one of the shopkeepers peers out from his shuttered windows. " +
                "Amazed to see you alive, he opens the window and calls out, " +
                "\"Is it over? Are we safe?\" " +
                "You wave to let him know that everything is fine.");
        addInfo("The Lead Investigator:\n" +
                "-[#GOOD]Gains one random Ally from the deck[]");
    }
}
