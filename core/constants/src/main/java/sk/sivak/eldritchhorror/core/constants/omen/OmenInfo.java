package sk.sivak.eldritchhorror.core.constants.omen;

/**
 * @author msivak
 */
public interface OmenInfo {

    OmenId getOmenId();

    OmenColor getOmenColor();

    int getTokensCount();
}
