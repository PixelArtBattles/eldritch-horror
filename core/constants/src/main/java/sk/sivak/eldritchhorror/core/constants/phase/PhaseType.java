package sk.sivak.eldritchhorror.core.constants.phase;

public enum PhaseType {
    ACTION,
    ENCOUNTER,
    MYTHOS,
}
