package sk.sivak.eldritchhorror.core.constants.reference;

/**
 * @author msivak
 */
public interface ReferenceInfo {

    int getPlayers();

    int getSpawnGates();

    int getSpawnClues();

    int getMonsterSurge();
}
