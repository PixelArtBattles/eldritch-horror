package sk.sivak.eldritchhorror.core.constants.spell;

public interface SpellBack {
    SpellEffect getSpellEffect(int rolledValue);

    int getSpellBackId();
}
