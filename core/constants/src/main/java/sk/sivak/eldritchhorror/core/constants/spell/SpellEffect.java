package sk.sivak.eldritchhorror.core.constants.spell;

public interface SpellEffect {
    String getFlavorText();

    int getSpellEffectId();
}
