package sk.sivak.eldritchhorror.core.constants.test;

public enum TestFlavorType {
    EMPTY,
    SPELL,
    ACQUIRE_ASSETS,
    COMBAT,
    CONDITION,
    RESEARCH,
    OTHER_WORLD,
    EXPEDITION,
    MYSTIC_RUINS
}
