package sk.sivak.eldritchhorror.core.service;

public interface HasManyValues<T> {

    T[] values();

}
