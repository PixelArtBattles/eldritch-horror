package sk.sivak.eldritchhorror.core.util;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import java8.features.function.Supplier;
import rx.Completable;
import rx.schedulers.Schedulers;
import sk.sivak.eldritchhorror.core.constants.ViewProperties;
import sk.sivak.eldritchhorror.core.constants.tracker.AnalyticsCategory;
import sk.sivak.eldritchhorror.core.constants.tracker.GoogleServicesHolder;
import sk.sivak.eldritchhorror.core.view.components.tutorial.Chalkboard;
import sk.sivak.eldritchhorror.core.view.game.InfoStage;
import sk.sivak.eldritchhorror.core.view.trello.TrelloUtils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

public class GlobalThrowableHandler {

    private final Skin skin;
    private boolean exceptionHandled = false;
    private Supplier<List<String>> lastCommandsInQueueSupplier;
    private String sStackTrace;

    public GlobalThrowableHandler(Skin skin) {
        this.skin = skin;
    }

    public void handleThrowable(Throwable t) {
        if (Gdx.app.getType() == Application.ApplicationType.Desktop) {
            t.printStackTrace(System.err);
            return;
        }
        if (exceptionHandled) {
            return;
        }
        exceptionHandled = true;

        GoogleServicesHolder.getAnalyticsTracker().trackInteraction(AnalyticsCategory.EXCEPTION, t.toString());
        sendNote(t);

    }

    public void setLastCommandsInQueueSupplier(Supplier<List<String>> lastCommandsInQueueSupplier) {
        this.lastCommandsInQueueSupplier = lastCommandsInQueueSupplier;
    }

    private void sendNote(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);
        sStackTrace = sw.toString();


        Chalkboard chalkboard = new Chalkboard(null);
        chalkboard.setPosition(
                ViewProperties.VIEWPORT_WIDTH / 2f - chalkboard.getWidth() / 2f,
                ViewProperties.VIEWPORT_HEIGHT / 2f - chalkboard.getHeight() / 2f);
        InfoStage.getChalkboardLayer().addActor(chalkboard);
        Completable displayChalkboard = chalkboard.display("[RED]Ooops![]\n \nSomething went wrong.\nI will fix it ASAP.\n \nSorry :(");

        List<String> listOfCommands = lastCommandsInQueueSupplier.get();
        StringBuilder commands = new StringBuilder();
        for (int i = Math.min(10, listOfCommands.size()-1); i >= 0; i--) {
            commands.append("-> ").append(listOfCommands.get(i)).append("\n");
        }

        TrelloUtils.createScreenshot();

        TrelloUtils.createExceptionCard(t.toString()).subscribeOn(Schedulers.io()).subscribe(cardId -> {
            Completable.merge(
                    displayChalkboard,
                    TrelloUtils.addComment(cardId, sStackTrace).observeOn(Schedulers.io()),
                    TrelloUtils.addComment(cardId, commands.toString()).observeOn(Schedulers.io()),
                    TrelloUtils.addAttachment(cardId, Gdx.files.local("save.json").file()).observeOn(Schedulers.io()),
                    TrelloUtils.addAttachment(cardId, Gdx.files.local("screenshot.png").file()).observeOn(Schedulers.io())
            ).subscribe(() -> {
                GoogleServicesHolder.getAnalyticsTracker().uncaughtException(Thread.currentThread(), t);
            });


        });
    }
}
