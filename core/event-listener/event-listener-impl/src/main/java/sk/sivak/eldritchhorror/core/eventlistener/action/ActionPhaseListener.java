package sk.sivak.eldritchhorror.core.eventlistener.action;

import sk.sivak.eldritchhorror.core.eventqueue.EventListener;
import sk.sivak.eldritchhorror.core.eventtype.data.CollectAvailableActionsData;

/**
 * @author msivak
 */
public interface ActionPhaseListener extends EventListener<CollectAvailableActionsData> {
}
