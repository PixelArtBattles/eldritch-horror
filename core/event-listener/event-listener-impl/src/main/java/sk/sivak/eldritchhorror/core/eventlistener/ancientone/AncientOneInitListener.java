package sk.sivak.eldritchhorror.core.eventlistener.ancientone;

/**
 * @author msivak
 */
public abstract class AncientOneInitListener {

    public abstract void register();

    public abstract void load();
}
