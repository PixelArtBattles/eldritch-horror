package sk.sivak.eldritchhorror.core.eventlistener.back.condition.amnesia;

import sk.sivak.eldritchhorror.core.constants.condition.ConditionId;
import sk.sivak.eldritchhorror.core.constants.condition.ConditionInfo;

public class AmnesiaConditionBackListener2 extends AbstractAmnesiaConditionBackListener {

    public AmnesiaConditionBackListener2(ConditionInfo conditionInfo) {
        super(conditionInfo, ConditionId.DARK_PACT);
    }
}
