package sk.sivak.eldritchhorror.core.eventlistener.back.condition.amnesia;

import sk.sivak.eldritchhorror.core.constants.condition.ConditionId;
import sk.sivak.eldritchhorror.core.constants.condition.ConditionInfo;

public class AmnesiaConditionBackListener3 extends AbstractAmnesiaConditionBackListener {

    public AmnesiaConditionBackListener3(ConditionInfo conditionInfo) {
        super(conditionInfo, ConditionId.DETAINED);
    }
}
