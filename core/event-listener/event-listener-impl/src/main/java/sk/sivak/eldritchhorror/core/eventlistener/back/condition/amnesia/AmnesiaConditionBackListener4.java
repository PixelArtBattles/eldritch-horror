package sk.sivak.eldritchhorror.core.eventlistener.back.condition.amnesia;

import sk.sivak.eldritchhorror.core.constants.condition.ConditionId;
import sk.sivak.eldritchhorror.core.constants.condition.ConditionInfo;

public class AmnesiaConditionBackListener4 extends AbstractAmnesiaConditionBackListener {

    public AmnesiaConditionBackListener4(ConditionInfo conditionInfo) {
        super(conditionInfo, ConditionId.CURSED);
    }
}
