package sk.sivak.eldritchhorror.core.eventlistener.encounter.builder;

public interface ComplexEncounterTextBuilder {
    void addPassPrefix();

    void addFailPrefix();
}
