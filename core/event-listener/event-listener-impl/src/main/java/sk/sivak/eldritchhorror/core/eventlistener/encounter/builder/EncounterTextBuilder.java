package sk.sivak.eldritchhorror.core.eventlistener.encounter.builder;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class EncounterTextBuilder {

    private static final Logger logger = LogManager.getLogger(EncounterTextBuilder.class);

    protected Properties properties;

    private String resourceKey = "";

    protected static Map<String, Properties> propertiesMap = new HashMap<>();
    protected int page;

    protected void resetKey() {
        resourceKey = "";
        withPage(page);
    }

    protected void withPage(int page) {
        appendToResourceKey(String.valueOf(page));
    }

    public EncounterTextBuilder withFlavor() {
        appendToResourceKey(".flavor");
        return this;
    }

    public EncounterTextBuilder withFlavor(int flavorNr) {
        appendToResourceKey(".flavor." + flavorNr);
        return this;
    }

    public EncounterTextBuilder withPass() {
        appendToResourceKey(".pass");
        return this;
    }

    public EncounterTextBuilder withFail() {
        appendToResourceKey(".fail");
        return this;
    }

    public EncounterTextBuilder withOption(int optionNr) {
        appendToResourceKey(".option."+optionNr);
        return this;
    }

    public EncounterTextBuilder withInfo() {
        appendToResourceKey(".info");
        return this;
    }

    public EncounterTextBuilder withInfo(int infoNr) {
        appendToResourceKey(".info."+infoNr);
        return this;
    }

    public EncounterTextBuilder withQuestion() {
        appendToResourceKey(".question");
        return this;
    }

    void appendToResourceKey(String value) {
        resourceKey = resourceKey + value;
    }

    public String build() {
        String resourceKey = getResourceKey();
        String property = properties.getProperty(resourceKey);
        if (property == null) {
            logger.error("Property not found for key: '"+resourceKey+"'");
        }
        resetKey();
        return property;
    }

    public String getResourceKey() {
        return resourceKey;
    }
}
