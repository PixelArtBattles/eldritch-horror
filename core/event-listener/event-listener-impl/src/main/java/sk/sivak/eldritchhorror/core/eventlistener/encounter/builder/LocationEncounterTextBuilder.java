package sk.sivak.eldritchhorror.core.eventlistener.encounter.builder;

import sk.sivak.eldritchhorror.core.constants.encounter.EncounterType;
import sk.sivak.eldritchhorror.core.constants.location.LocationType;
import sk.sivak.eldritchhorror.core.eventtype.data.encounter.LocationEncounter;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

public class LocationEncounterTextBuilder extends EncounterTextBuilder {

    public LocationEncounterTextBuilder(int page, LocationEncounter.LocationEncounterType locationType) {
        this.page = page;
        if (propertiesMap.get(locationType.name()) != null) {
            properties = propertiesMap.get(locationType.name());
        } else {
            initProperties(locationType);
        }

        withPage(page);
    }

    private void initProperties(LocationEncounter.LocationEncounterType locationType) {
        String fileName = "encounter/"+locationType.name()+".properties";

        URL resource = Thread.currentThread().getContextClassLoader().getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("Resource was null");
        }
        properties = new Properties();
        try {
            properties.load(resource.openStream());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        propertiesMap.put(locationType.name(), properties);
    }

    @Override
    protected void resetKey() {
        super.resetKey();
    }
}
