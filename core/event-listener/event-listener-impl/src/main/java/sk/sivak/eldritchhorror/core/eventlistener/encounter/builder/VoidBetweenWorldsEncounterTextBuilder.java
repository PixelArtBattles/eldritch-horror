package sk.sivak.eldritchhorror.core.eventlistener.encounter.builder;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

public class VoidBetweenWorldsEncounterTextBuilder extends EncounterTextBuilder implements ComplexEncounterTextBuilder{

    private static final String VOID_BETWEEN_WORLDS = "VoidBetweenWorlds";
    private String prefix = "";

    public VoidBetweenWorldsEncounterTextBuilder(int page) {
        this.page = page;
        if (propertiesMap.get(VOID_BETWEEN_WORLDS) != null) {
            properties = propertiesMap.get(VOID_BETWEEN_WORLDS);
        } else {
            initProperties();
        }

        withPage(page);
    }

    private void initProperties() {
        String fileName = "encounter/void_between_worlds.properties";

        URL resource = Thread.currentThread().getContextClassLoader().getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("Resource was null");
        }
        properties = new Properties();
        try {
            properties.load(resource.openStream());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        propertiesMap.put(VOID_BETWEEN_WORLDS, properties);
    }

    private void withPrefix() {
        appendToResourceKey(prefix);
    }

    @Override
    protected void resetKey() {
        super.resetKey();
        withPrefix();
    }

    @Override
    public void addPassPrefix() {
        prefix = ".pass";
        resetKey();
    }

    @Override
    public void addFailPrefix() {
        prefix = ".fail";
        resetKey();
    }
}
