package sk.sivak.eldritchhorror.core.eventlistener.encounter.template;

public interface EncounterTemplate {

    void execute();
}
