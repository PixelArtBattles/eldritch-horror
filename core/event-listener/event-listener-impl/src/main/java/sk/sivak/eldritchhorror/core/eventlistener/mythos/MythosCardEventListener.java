package sk.sivak.eldritchhorror.core.eventlistener.mythos;

public interface MythosCardEventListener {

    void execute();
}
