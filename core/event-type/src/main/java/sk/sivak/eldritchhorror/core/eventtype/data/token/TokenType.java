package sk.sivak.eldritchhorror.core.eventtype.data.token;

public enum TokenType {
    HEALTH,
    SANITY,
    CLUE,
    FOCUS
}
