package sk.sivak.eldritchhorror.core.model;

import sk.sivak.eldritchhorror.core.constants.ancientone.AncientOneInfo;

/**
 * @author msivak
 */
public interface AncientOneRead {

    AncientOneInfo getAncientOneInfo();
}
