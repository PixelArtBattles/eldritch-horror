package sk.sivak.eldritchhorror.core.model;

/**
 * @author msivak
 */
public interface DoomTrackRead {

    int getCurrentDoom();
}
