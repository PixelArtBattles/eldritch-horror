package sk.sivak.eldritchhorror.core.model;

import sk.sivak.eldritchhorror.core.constants.phase.PhaseType;

public interface PhaseRead {
    PhaseType getPhaseType();
}
