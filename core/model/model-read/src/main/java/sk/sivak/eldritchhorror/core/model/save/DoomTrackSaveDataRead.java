package sk.sivak.eldritchhorror.core.model.save;

public interface DoomTrackSaveDataRead {
    int getCurrentDoom();
}
