package sk.sivak.eldritchhorror.core.model.save;

import sk.sivak.eldritchhorror.core.constants.phase.PhaseType;

public interface PhaseSaveDataRead {
    PhaseType getCurrentPhase();
}
