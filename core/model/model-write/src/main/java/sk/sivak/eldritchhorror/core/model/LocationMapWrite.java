package sk.sivak.eldritchhorror.core.model;

/**
 * @author msivak
 */
public interface LocationMapWrite extends LocationMapRead {

    void initLocations();
}
