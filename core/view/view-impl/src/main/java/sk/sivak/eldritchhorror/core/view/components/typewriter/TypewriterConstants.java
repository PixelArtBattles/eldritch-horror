package sk.sivak.eldritchhorror.core.view.components.typewriter;

public class TypewriterConstants {

    static final float FONT_SCALE = 0.35f;
    static final float TEXT_AREA_WIDTH = 400f;
    static final int BACKGROUND_WIDTH = 540;
    static final int BACKGROUND_HEIGHT = 960;
    static final int TEXT_AREA_PAD_TOP = 60;
    static final float TABLE_SCALE = 1.25f;

    static final String BAD_PLACEHOLDER = "\\[#BAD]";
    static final String GOOD_PLACEHOLDER = "\\[#GOOD]";
    static final float NEW_LINE_SPEED = 0.1f;
    static final String HEX_BAD = "e02323";
    static final String HEX_GOOD = "229B3C";
    static final float TEXT_LINE_HEIGHT = 60 * FONT_SCALE;
    static final float BUTTON_HEIGHT = TEXT_LINE_HEIGHT * 2.5f;
    static final float BUTTON_BORDER = 10f / FONT_SCALE;

    static final float MIN_BUTTON_WIDTH = 100f;
}
