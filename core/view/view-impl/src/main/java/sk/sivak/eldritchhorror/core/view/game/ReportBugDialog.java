package sk.sivak.eldritchhorror.core.view.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import rx.Completable;
import rx.schedulers.Schedulers;
import sk.sivak.eldritchhorror.core.constants.ViewProperties;
import sk.sivak.eldritchhorror.core.constants.tracker.GoogleServicesHolder;
import sk.sivak.eldritchhorror.core.view.assetmanager.CustomAssetManager;
import sk.sivak.eldritchhorror.core.view.trello.TrelloUtils;
import sk.sivak.eldritchhorror.core.view.utils.ButtonUtils;

import java.util.List;

import static sk.sivak.eldritchhorror.core.view.assetmanager.CustomAssetManager.FONT_ADLER;

public class ReportBugDialog extends Dialog {

    private final MenuButton menuButton;
    private final TextArea textArea;
    private final TextButton sendButton;
    private final TextButton closeButton;

    public ReportBugDialog(MenuButton menuButton, Skin skin) {
        super(" Report a Bug", skin);
        this.menuButton = menuButton;

        textArea = new TextArea("Describe your problem...", skin);
        sendButton = createNiceButton("Send", skin);
        closeButton = createNiceButton("Close", skin);


        getContentTable().add(textArea).pad(5).size(ViewProperties.VIEWPORT_WIDTH * 0.5f, ViewProperties.VIEWPORT_HEIGHT * 0.5f);
        getButtonTable().add(sendButton).pad(5);
        getButtonTable().add(closeButton).pad(5);
        pack();

        ButtonUtils.addClickListener(closeButton, () -> {
            hide(Actions.run(() -> {
                MapStage.brightenWorld();
                menuButton.menuButtonClickable = true;
                menuButton.getColor().a = 1f;
            }));
        });

        ButtonUtils.addClickListener(sendButton, this::reportBug);
    }

    private TextButton createNiceButton(String text, Skin skin) {
        TextButton niceButton = new TextButton(text, skin);
        niceButton.getLabel().setFontScale(0.5f);
        niceButton.setSize(280, 50);
        niceButton.getLabel().setStyle(new Label.LabelStyle(CustomAssetManager.getBitmapFont(FONT_ADLER), Color.WHITE));
        return niceButton;
    }

    private void reportBug() {
        TrelloUtils.createBugCard("[1.7.11] Bug Report!").subscribeOn(Schedulers.io()).subscribe(cardId -> {
            Completable.merge(
                    TrelloUtils.addComment(cardId, textArea.getText()).observeOn(Schedulers.io()),
                    TrelloUtils.addAttachment(cardId, Gdx.files.local("save.json").file()).observeOn(Schedulers.io())
            ).subscribe(() -> {
                hide(Actions.run(() -> {
                    MapStage.brightenWorld();
                    menuButton.menuButtonClickable = true;
                    menuButton.getColor().a = 1f;
                }));
            });
        });
    }
}
