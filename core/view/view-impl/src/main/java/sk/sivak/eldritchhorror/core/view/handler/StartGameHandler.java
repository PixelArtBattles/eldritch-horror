package sk.sivak.eldritchhorror.core.view.handler;

/**
 * @author msivak
 */
public interface StartGameHandler {

    void startGame();
}
