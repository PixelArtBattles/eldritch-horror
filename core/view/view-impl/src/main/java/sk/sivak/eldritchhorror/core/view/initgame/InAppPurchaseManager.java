package sk.sivak.eldritchhorror.core.view.initgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.pay.Transaction;
import rx.Single;
import rx.functions.Action0;
import rx.functions.Action1;
import sk.sivak.eldritchhorror.core.constants.tracker.GoogleServicesHolder;
import sk.sivak.eldritchhorror.core.view.firebase.FirebasePurchase;

public class InAppPurchaseManager {
    private Action1<Throwable> handleRestoreErrorAction;
    private Action1<Transaction[]> handleRestoreAction;

    private Action1<Transaction> handlePurchaseAction;
    private Action0 handlePurchaseCanceledAction;
    private Action1<Throwable> handlePurchaseErrorAction;


    public Single<Boolean> isProductPurchased(String productName) {
        return Single.<Boolean>create(onSub -> {
            Preferences preferences = Gdx.app.getPreferences("AncientTerror.xml");

            if (preferences.contains(productName)) {
                onSub.onSuccess(preferences.getBoolean(productName, false));
                return;
            }

            handleRestoreAction = transactions -> {
                for (Transaction transaction : transactions) {
                    if (!productName.equals(transaction.getIdentifier())) {
                        continue;
                    }
                    preferences.putBoolean(productName, true);
                    preferences.flush();
                    onSub.onSuccess(transaction.isPurchased());
                    return;
                }
                preferences.putBoolean(productName, false);
                preferences.flush();
                onSub.onSuccess(false);
            };


            handleRestoreErrorAction = error -> {
                onSub.onSuccess(false);
            };

            if (GoogleServicesHolder.getCustomPurchaseObserver() != null) {
                GoogleServicesHolder.getCustomPurchaseObserver().addHandleRestoreAction(handleRestoreAction);
                GoogleServicesHolder.getCustomPurchaseObserver().addHandleRestoreErrorAction(handleRestoreErrorAction);
            }

            try {
                GoogleServicesHolder.getPurchaseManager().purchaseRestore();
            } catch (Exception e) {
                onSub.onSuccess(false);
            }

        }).doOnSuccess(value -> {
            if (GoogleServicesHolder.getCustomPurchaseObserver() != null) {
                GoogleServicesHolder.getCustomPurchaseObserver().removeHandleRestoreAction(handleRestoreAction);
                GoogleServicesHolder.getCustomPurchaseObserver().removeHandleRestoreErrorAction(handleRestoreErrorAction);
            }
        });
    }

    public Single<Boolean> purchaseProduct(String productName) {
        return Single.<Boolean>create(onSub -> {
            handlePurchaseAction = transaction -> {
                Preferences preferences = Gdx.app.getPreferences("AncientTerror.xml");
                preferences.putBoolean(productName, transaction.isPurchased());
                preferences.flush();
                onSub.onSuccess(transaction.isPurchased());
                new FirebasePurchase().recordPurchase(productName);
            };

            handlePurchaseCanceledAction = () -> {
                onSub.onSuccess(false);
            };

            handlePurchaseErrorAction = throwable -> {
                onSub.onSuccess(false);
            };


            GoogleServicesHolder.getCustomPurchaseObserver().addHandlePurchaseAction(handlePurchaseAction);
            GoogleServicesHolder.getCustomPurchaseObserver().addHandlePurchaseCanceledAction(handlePurchaseCanceledAction);
            GoogleServicesHolder.getCustomPurchaseObserver().addHandlePurchaseErrorAction(handlePurchaseErrorAction);

            try {
                GoogleServicesHolder.getPurchaseManager().purchase(productName);
            } catch (Exception e) {
                onSub.onSuccess(false);
            }


        }).doOnSuccess(value -> {
            GoogleServicesHolder.getCustomPurchaseObserver().removeHandlePurchaseAction(handlePurchaseAction);
            GoogleServicesHolder.getCustomPurchaseObserver().removeHandlePurchaseCanceledAction(handlePurchaseCanceledAction);
            GoogleServicesHolder.getCustomPurchaseObserver().removeHandlePurchaseErrorAction(handlePurchaseErrorAction);
        });
    }
}
