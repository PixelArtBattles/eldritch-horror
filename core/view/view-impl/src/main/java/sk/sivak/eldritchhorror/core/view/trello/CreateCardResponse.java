package sk.sivak.eldritchhorror.core.view.trello;

public class CreateCardResponse {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
