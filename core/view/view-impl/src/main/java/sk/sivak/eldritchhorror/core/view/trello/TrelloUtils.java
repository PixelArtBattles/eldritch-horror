package sk.sivak.eldritchhorror.core.view.trello;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.net.HttpParametersUtils;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.ScreenUtils;
import rx.Completable;
import rx.Single;
import rx.subjects.PublishSubject;

import java.io.File;
import java.util.HashMap;

public class TrelloUtils {

    public static final String CREATE_CARD_URL = "https://api.trello.com/1/cards";
    public static final String KEY = "ae5a55fcde99966eb4052ddc13338bec";
    public static final String TOKEN = "14d4e839cecee2edbc1a30ca6d48b4b9f58be4c21c69351e05cf2988a6d3367d";


    public static Single<String> createExceptionCard(String name) {
        return createCard("5ccd7261835afb425b6c21d0", name);
    }

    public static Single<String> createBugCard(String name) {
        return createCard("5d9c991c1ab4a524fdd4baa7", name);
    }

    public static Single<String> createCard(String idList, String name) {
        PublishSubject<String> publishSubject = PublishSubject.<String>create();
        Net.HttpRequest request = new Net.HttpRequest("POST");
        request.setUrl(CREATE_CARD_URL);
        HashMap<String, String> parametersMap = new HashMap<>();
        parametersMap.put("name",name);
        parametersMap.put("idList", idList);
        parametersMap.put("key",KEY);
        parametersMap.put("token",TOKEN);
        String convertedParameters = HttpParametersUtils.convertHttpParameters(parametersMap);
        request.setUrl(request.getUrl() + "?"+ convertedParameters);

        Json json = new Json();
        json.setIgnoreUnknownFields(true);
        Gdx.net.sendHttpRequest(request, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
                String result = httpResponse.getResultAsString();
                CreateCardResponse response = json.fromJson(CreateCardResponse.class, result);
                publishSubject.onNext(response.getId());
                publishSubject.onCompleted();
            }

            @Override
            public void failed(Throwable t) {
                publishSubject.onError(t);
            }

            @Override
            public void cancelled() {
                publishSubject.onCompleted();
            }
        });
        return publishSubject.toSingle();
    }

    public static Completable addComment(String cardId, String text) {
        PublishSubject<Object> publishSubject = PublishSubject.create();
        Net.HttpRequest request = new Net.HttpRequest("POST");
        request.setUrl("https://api.trello.com/1/cards/"+cardId+"/actions/comments");
        HashMap<String, String> parametersMap = new HashMap<>();
        parametersMap.put("text",text);
        parametersMap.put("key", KEY);
        parametersMap.put("token", TOKEN);
        String convertedParameters = HttpParametersUtils.convertHttpParameters(parametersMap);
        request.setUrl(request.getUrl() + "?"+ convertedParameters);

        Json json = new Json();
        json.setIgnoreUnknownFields(true);
        Gdx.net.sendHttpRequest(request, new Net.HttpResponseListener() {
            @Override
            public void handleHttpResponse(Net.HttpResponse httpResponse) {
                publishSubject.onCompleted();
            }

            @Override
            public void failed(Throwable t) {
                publishSubject.onError(t);
            }

            @Override
            public void cancelled() {
                publishSubject.onCompleted();
            }
        });
        return publishSubject.toCompletable();
    }

    public static Completable addAttachment(String cardId, File file) {
        if (!file.exists()) {
            return Completable.complete();
        }
        try {
            HashMap<String, String> parametersMap = new HashMap<>();
            parametersMap.put("key", KEY);
            parametersMap.put("token", TOKEN);
            String convertedParameters = HttpParametersUtils.convertHttpParameters(parametersMap);

            MultipartUtility multipartUtility = new MultipartUtility(
                    "https://api.trello.com/1/cards/" + cardId + "/attachments?" + convertedParameters,
                    "UTF-8");
            multipartUtility.addFilePart("file", file);
            multipartUtility.finish();
            return Completable.complete();
        } catch (Exception e) {
            return Completable.error(e);
        }
    }


    public static void createScreenshot() {
        byte[] pixels = ScreenUtils.getFrameBufferPixels(0, 0, Gdx.graphics.getBackBufferWidth(), Gdx.graphics.getBackBufferHeight(), true);

        // this loop makes sure the whole screenshot is opaque and looks exactly like what the user is seeing
        for(int i = 4; i < pixels.length; i += 4) {
            pixels[i - 1] = (byte) 255;
        }

        Pixmap pixmap = new Pixmap(Gdx.graphics.getBackBufferWidth(), Gdx.graphics.getBackBufferHeight(), Pixmap.Format.RGBA8888);
        BufferUtils.copy(pixels, 0, pixmap.getPixels(), pixels.length);
        PixmapIO.writePNG(Gdx.files.local("screenshot.png"), pixmap);
        pixmap.dispose();
    }
}
