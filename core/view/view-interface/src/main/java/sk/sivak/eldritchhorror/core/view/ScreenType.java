package sk.sivak.eldritchhorror.core.view;

/**
 * @author msivak
 */
public enum ScreenType {
    INIT_GAME,
    GAME,
    CARDS,
    HALL_OF_FAME
}
