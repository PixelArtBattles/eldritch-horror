package java8.features.function;

public interface BiConsumer<T, U> {
    void accept(T t, U u);
}
