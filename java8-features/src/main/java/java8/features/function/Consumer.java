package java8.features.function;

public interface Consumer<T> {
    void accept(T t);
}
