package java8.features.function;

public interface Function<T, R> {
    R apply(T t);
}
