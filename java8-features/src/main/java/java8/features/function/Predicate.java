package java8.features.function;

public interface Predicate<T> {
    boolean test(T t);
}
