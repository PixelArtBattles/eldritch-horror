package java8.features.function;

public interface Supplier<T> {
    T get();
}
